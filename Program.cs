﻿using RPG_characters.Characters;
using RPG_characters.Equipment;
using RPG_characters.Stats;
using System;

namespace RPG_characters
{
    class Program
    {
        static void Main(string[] args)
        {
            PrimaryAttribute weaponstat = new PrimaryAttribute(1, 1, 1);
            Mage Morgana = new Mage("Morgana");
            Armor Robe = new Armor("Robe", 1, ItemSlots.BODY, weaponstat, ArmorType.CLOTH);
            Weapons ElderWand = new Weapons("The Elder wand", 2, ItemSlots.WEAPON, WeaponType.WAND, 1, 8);
            Morgana.LevelUp();
            Console.WriteLine(Morgana.DisplayCharacter());
            Warrior Olaf = new Warrior("Olaf");
        }
    }
}
