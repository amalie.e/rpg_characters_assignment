﻿using RPG_characters.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Equipment
{
    public enum WeaponType
    {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }
    public class Weapons: Items
    {
        public WeaponType Weapon { get; set; }
        public int Damage { get; set; }
        public float AttackSpeed { get; set; }

        // Constructor
        public Weapons(string Name, int Level, ItemSlots Slot, WeaponType Weapon, int Damage, float AttackSpeed): base(Name, Level, Slot)
        {
            this.Weapon = Weapon;
            this.Damage = Damage;
            this.AttackSpeed = AttackSpeed;
        }
        // Method for damage times attackspeed
        public float Dps()
        {
            return Damage * AttackSpeed;
        }
    }
}
