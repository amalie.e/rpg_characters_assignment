﻿using RPG_characters.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Equipment
{
    public abstract class Items
    {
        public string Name { get; set; }
        public int LevelRequirement { get; set; }
        public ItemSlots Slot { get; set; }

        // Constructor
        public Items(string Name, int LevelRequirement, ItemSlots Slot)
        {
            this.Name = Name;
            this.LevelRequirement = LevelRequirement;
            this.Slot = Slot;
        }


        
        // Method for displaying the items
        public string DisplayItems()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Item Name: " + this.Name);
            sb.AppendLine("Required Level: " + this.LevelRequirement);
            sb.AppendLine("Level: " + this.Slot);
            return sb.ToString();
        }
    }
}
