﻿using RPG_characters.Characters;
using RPG_characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Equipment
{
    public enum ArmorType 
    {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
    public class Armor: Items
    {
        public PrimaryAttribute Stats { get; set; }
        public ArmorType ArmorTyp { get; set; }

        // Constructor
        public Armor(string Name, int Level, ItemSlots Slot, PrimaryAttribute Stats, ArmorType ArmorTyp): base(Name, Level, Slot)
        {
            this.ArmorTyp = ArmorTyp;
            this.Stats = Stats;
        }
    }
}
