﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Stats
{
    public class PrimaryAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        // Constructor
        public PrimaryAttribute(int strength, int dexterity, int intelligence)
        {
            this.Strength = strength;
            this.Dexterity = dexterity;
            this.Intelligence = intelligence;
        }

        // Overload of the + operator
        public static PrimaryAttribute operator +(PrimaryAttribute left, PrimaryAttribute right)
        {
            return new PrimaryAttribute(
                left.Strength += right.Strength,
                 left.Dexterity += right.Dexterity,
                  left.Intelligence += right.Intelligence

            );
        }
        }
}
