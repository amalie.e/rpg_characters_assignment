﻿using RPG_characters.Equipment;
using RPG_characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Characters
{
    public enum ItemSlots
    {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
    public abstract class Character
    {
        /// Attributes
        public PrimaryAttribute BaseStats { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }

        public PrimaryAttribute LevelUpStats { get; set; }
        public PrimaryAttribute TotalStats { get; set; }

        public float MainStats;
        public string MainStatsT;

        ///Items
        public WeaponType[] CanEquipWeapon = { };

        public ArmorType[] CanEquipArmor = { };

        public Dictionary<ItemSlots, Items> Equipped = new Dictionary<ItemSlots, Items>();

        // Method that calculates the damage done with weapon
        public Double DoDamage()
        {
            Double Weapondps = 1;
            if (Equipped[ItemSlots.WEAPON] != null)
            {
                Weapons weapons = (Weapons)Equipped[ItemSlots.WEAPON];
                Weapondps = weapons.Dps();
            }
            return Weapondps * (1 + (this.MainStats / 100));

        }

        // Method to check that the character can equip the weapon and is the right level
        public void Equip(Items gear)
        {
            if (gear.Slot == ItemSlots.WEAPON)
            {
            //    Weapons weapon = (Weapons)gear;
            }
        }

        // Method that updates the characters stats after leveling up and adding items
        public void UpdateStats()
        {
            this.TotalStats = new PrimaryAttribute(0, 0, 0);
            this.TotalStats = this.TotalStats + this.BaseStats;
            foreach (KeyValuePair<ItemSlots, Items> item in Equipped) 
            {
                if (item.Key!= ItemSlots.WEAPON)
                {
                    Armor armor = (Armor)item.Value;
                    this.TotalStats = this.TotalStats + armor.Stats;
                }
            }

            switch (this.MainStatsT)
            {
                case "inte":
                    this.MainStats = this.TotalStats.Intelligence;
                    break;
                case "stre":
                    this.MainStats = this.TotalStats.Strength;
                    break;
                case "dext":
                    this.MainStats = this.TotalStats.Dexterity;
                    break;
                default:
                    this.MainStats = 0;
                    break;
            }
        }

        /// Method that lets the character with items, stats and level be displayed in the console
        public string DisplayCharacter()
        {
            StringBuilder sb = new StringBuilder(70);
            sb.AppendLine("Name: " + this.Name);
            sb.AppendLine("Level: " + this.Level);
            sb.AppendLine("Strength: " + this.TotalStats.Strength);
            sb.AppendLine("Dexterity: " + this.TotalStats.Dexterity);
            sb.AppendLine("Intelligence: " + this.TotalStats.Intelligence);
            sb.AppendLine("Damage: " + this.DoDamage());

            return sb.ToString();
        }

        // Method that lets the character level up and increases stats with leveling
        public void LevelUp()
        {
            this.Level = Level + 1;
            this.BaseStats += this.LevelUpStats;
            Console.WriteLine(this.Level.ToString());
            UpdateStats();
        }

        // Throws custom exception if character tries to equip a weapon it can not
        // Did not get to throw these errors as i did not finish my equip method
       // public class InvalidWeaponException: Exception
        //{
          //  public InvalidWeaponException(string: message) { }
        //}
        /// Trows custom exception if character tries to equip an armor it can not
        //public class InvalidArmorException: Exception
        //{

        }

}
