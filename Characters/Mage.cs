﻿using RPG_characters.Equipment;
using RPG_characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Characters
{
    public class Mage: Character 
    {
        public Mage(string Name)
        {
            this.Name = Name;
            this.Level = 1;
            this.BaseStats = new PrimaryAttribute(1, 1, 8);
            this.LevelUpStats = new PrimaryAttribute(1, 1, 5);

            // Equipment
            this.CanEquipArmor = new ArmorType[] { ArmorType.CLOTH };
            this.CanEquipWeapon = new WeaponType[] { WeaponType.STAFF, WeaponType.WAND };
            this.Equipped = new Dictionary<ItemSlots, Items> { { ItemSlots.WEAPON, null } };

            this.UpdateStats();
            this.MainStatsT = "intel";
        }
    }
}
