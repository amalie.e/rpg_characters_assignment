﻿using RPG_characters.Equipment;
using RPG_characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Characters
{
    public class Rouge: Character
    {
        public Rouge(string Name)
        {
            this.Name = Name;
            this.Level = 1;
            this.BaseStats = new PrimaryAttribute(2, 6, 1);
            this.LevelUpStats = new PrimaryAttribute(1, 4, 1);

            // Equipment
            this.CanEquipArmor = new ArmorType[] { ArmorType.LEATHER, ArmorType.MAIL };
            this.CanEquipWeapon = new WeaponType[] { WeaponType.DAGGER, WeaponType.SWORD };
            this.Equipped = new Dictionary<ItemSlots, Items> { { ItemSlots.WEAPON, null } };

            this.UpdateStats();
            this.MainStatsT = "dext";
        }
    }
}
