﻿using RPG_characters.Equipment;
using RPG_characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Characters
{
    public class Warrior: Character
    {
        public Warrior(string Name)
        {
            this.Name = Name;
            this.Level = 1;
            this.BaseStats = new PrimaryAttribute(5, 2, 1);
            this.LevelUpStats = new PrimaryAttribute(3, 2, 1);

            // Equipment
            this.CanEquipArmor = new ArmorType[] { ArmorType.MAIL, ArmorType.PLATE };
            this.CanEquipWeapon = new WeaponType[] { WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD };
            this.Equipped = new Dictionary<ItemSlots, Items> { { ItemSlots.WEAPON, null } };

            this.UpdateStats();
            this.MainStatsT = "stre";
        }
    } 
}
