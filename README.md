# RPG characters project #
This is an RPG project that has 4 classes, mage, ranger, rouge and warrior. The 
characters have levels, names, stats, weapons and armor. Characters can level up,
and increase stats and damage. 

## Using the project ##
Clone the repository. Open in
visual studio 2019.

## About ##
The project is created by Amalie Espeseth

## Project Status ##
I did not have time to finish my equip method. I prioratized some parts of the unit testing so i could include
it in the repo. As a result of that i also could not do any tests on the items.
I also had issues because my test project appeared outside the repo,
so i spent some time fixing that. I was going to use a dictionary to 
make sure the correct characters could equip only the armor and weapons
allowed in the equip method.