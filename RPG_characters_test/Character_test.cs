﻿using RPG_characters.Characters;
using RPG_characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPG_characters_test
{
    public class Character_test
    {
        [Fact] // Makes sure that character is level one when created
        public void Constructor_CheckLevel_BeLevelOne()
        {
            Warrior warrior = new Warrior("Appa");

            int expected = 1;
            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact] // Makes sure that characters next level is 2
        public void LevelUp_WarriorLevelOne_BeLevelTwo()
        {
            // Arrange
            Warrior warrior = new Warrior("Appa");
            warrior.LevelUp();
            int expected = 2;
            // Act
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact] // Makes sure character class has proper attributes when created
        public void Constructor_Mage_ProperAttributes()
        {
            // Arrange
            Mage mage = new Mage("Appa");
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(1, 1, 8);
            PrimaryAttribute actual = mage.BaseStats;
            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }
        [Fact] // Makes sure character class has proper attributes when created
        public void Constructor_Ranger_ProperAttributes()
        {
            // Arrange
            Ranger ranger = new Ranger("Appa");
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(1, 7, 1);
            PrimaryAttribute actual = ranger.BaseStats;
            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }
        [Fact] // Makes sure character class has proper attributes when created
        public void Constructor_Rouge_ProperAttributes()
        {
            // Arrange
            Rouge rouge = new Rouge("Appa");
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(2, 6, 1);
            PrimaryAttribute actual = rouge.BaseStats;
            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact] // Makes sure character class has proper attributes when created
        public void Constructor_Warrior_ProperAttributes()
        {
            // Arrange
            Warrior warrior = new Warrior("Appa");
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(5, 2, 1);
            PrimaryAttribute actual = warrior.BaseStats;
            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact] // Makes sure character class increases attributes when leveling up 
        public void LevelUpMethod_Mage_IncreasedAttributesWhenLevelingUp()
        {
            // Arrange
            Mage mage = new Mage("Appa");
            mage.LevelUp();
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(2, 2, 13);
            PrimaryAttribute actual = mage.TotalStats;
            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }
        [Fact] // Makes sure character class increases attributes when leveling up 
        public void LevelUpMethod_Ranger_IncreasedAttributesWhenLevelingUp()
        {
            // Arrange
            Ranger ranger = new Ranger("Appa");
            ranger.LevelUp();
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(2, 12, 2);
            PrimaryAttribute actual = ranger.TotalStats;
            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact] // Makes sure character class increases attributes when leveling up 
        public void LevelUpMethod_Rouge_IncreasedAttributesWhenLevelingUp()
        {
            // Arrange
            Rouge rouge = new Rouge("Appa");
            rouge.LevelUp();
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(3, 10, 2);
            PrimaryAttribute actual = rouge.TotalStats;
            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact] // Makes sure character class increases attributes when leveling up 
        public void LevelUpMethod_Warrior_IncreasedAttributesWhenLevelingUp()
        {
            // Arrange
            Warrior warrior = new Warrior("Appa");
            warrior.LevelUp();
            // Act
            PrimaryAttribute expected = new PrimaryAttribute(8, 4, 2);
            PrimaryAttribute actual = warrior.TotalStats;
            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }




    }
}
